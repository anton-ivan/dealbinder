# Dealbinder.com


An angular.js webapp build using Firebase.io

## Folder Structure

1. All app files live within the public > app folder
2. Everything external to this folder is the static marketing site
3. Documentation for the underlying theme/app can be found [here](http://pages.revox.io/dashboard/latest/angular/#/app/dashboard)


## Running locally

1. Run `npm install -g http-server'
2. Navigate to root of project
3. Run `http-server --cors -c-1 -o'
