'use strict';

/* Controllers */



angular.module('app')

    .controller('DashboardCtrl', ['$scope', '$http', '$timeout','$modal','FirebaseSvc', 'Auth','currentAuth',function($scope, $http, $timeout, $modal, FirebaseSvc,Auth, currentAuth) {

        $scope.modal = {};
        $scope.modal.slideUp = "default";
        $scope.modal.stickUp = "default";
        $scope.currentOrderId = 0;
        $scope.orderDetails = [];
        $scope.toggleSlideUpSize = function() {
            var size = $scope.modal.slideUp;
            var modalElem = $('#modalSlideUp');
            if (size == "mini") {
                $('#modalSlideUpSmall').modal('show')
            } else {
                $('#modalSlideUp').modal('show')
                if (size == "default") {
                    modalElem.children('.modal-dialog').removeClass('modal-lg');
                } else if (size == "full") {
                    modalElem.children('.modal-dialog').addClass('modal-lg');
                }
            }
        };

        $scope.stickUpSizeToggler = function() {
            var size = $scope.modal.stickUp;
            var modalElem = $('#myModal');
            if (size == "mini") {
                $('#modalStickUpSmall').modal('show')
            } else {
                $('#myModal').modal('show')
                if (size == "default") {
                    modalElem.children('.modal-dialog').removeClass('modal-lg');
                } else if (size == "full") {
                    modalElem.children('.modal-dialog').addClass('modal-lg');
                }
            }
        };

        $scope.modalSlideLeft = function() {
            setTimeout(function() {
                $('#modalSlideLeft').modal('show');
            }, 300);
        };

        $scope.fillSizeToggler = function() {
            $('#modalFillIn').modal('show');

            // Only for fillin modals so that the backdrop action is still there
            $('#modalFillIn').on('show.bs.modal', function(e) {
                $('body').addClass('fill-in-modal');
            });
            $('#modalFillIn').on('hidden.bs.modal', function(e) {
                $('body').removeClass('fill-in-modal');
            });

        }
        // open profile Menu
        $scope.openMenu = function() {
          alert('testing');
        }
        //load Firebase data
        $scope.initOrders = function() {
          //check firebase Auth
          console.log('current Auth');
          console.log(currentAuth);
          //get firebase userid
          var userId = currentAuth.uid;
          console.log(userId);
          FirebaseSvc.init(userId)
          .then(bindProfile)
          .then(bindOrders)
          .finally(function(){
            $scope.goToOrder($scope.currentOrderId);
            console.log('populated all information');
          });

        }
        //bind orders
        function bindOrders(){
          var obj = FirebaseSvc.getOrders();
          $scope.orders = obj;
          obj.$bindTo($scope,'orders');
          obj.$watch(function(evt){
            console.log('evt',evt);
            angular.forEach($scope.orders, function(order ,key){
              $scope.currentOrderId = key;
              order.status = (order.is_complete == true) ? 'Completed': 'In Progress';
            });
            console.log($scope.orders);
          });
          return obj.$loaded();
        }

        //bind orders
        function bindProfile(){
          var obj = FirebaseSvc.getUserProfile();
          $scope.myself = obj;
          obj.$bindTo($scope,'myself');
          obj.$watch(function(evt){
            console.log($scope.myself);
          });
          return obj.$loaded();
        }
        //goes to order ID
        //populate order Details
        $scope.goToOrder = function (orderId){
          $scope.orderDetails = [];
          angular.forEach($scope.orders[orderId], function(event, key){
            if(typeof event == 'object'){
              //first group by event name
              var isNew = true;
              angular.forEach($scope.orderDetails, function(entity){
                if(entity.entity_name == event.entity_name){
                     event.key = key;
                     entity.events.push(event);
                     entity.last_updated = event.last_updated;
                     if(!event.is_complete) entity.status = 'In Progres';
                     isNew = false;
                }
              });
              //if not found, new
              if(isNew){
                //add new root
                var newRoot = {
                  entity_name: event.entity_name,
                  status: 'Completed',
                  events: [event]
                };
                $scope.orderDetails.push(newRoot);
              }
              //add details
            }
          });
          $scope.currentOrderId = orderId;
          console.log('order Details');
          console.log($scope.orderDetails);
        };

        //open dashboard entity
        $scope.openEntityModal = function ( entity) {

          var modalInstance = $modal.open({
            templateUrl: 'tpl/blocks/entity_notes_modal.html',
            controller: 'EntityModalCtrl',
            size: 'md',
            resolve: {
              entity: function () {
                return entity;
              }
            }
          });

          modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
          }, function (reason) {
            console.log(reason);
          });
        };


        //open user profile
        $scope.openOrderModal = function (size) {

          var modalInstance = $modal.open({
            templateUrl: 'tpl/blocks/order_modal.html',
            controller: 'OrderModalCtrl',
            size: 'md',
            resolve: {
              profile: function () {
                return $scope.myself;
              }
            }
          });

          modalInstance.result.then(function (updatedProfile) {
            $scope.myself = updatedProfile;
          }, function (reason) {
            console.log(reason);
          });
        };


        $scope.initOrders();

        }]);
