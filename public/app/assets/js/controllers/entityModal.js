'use strict';

/* Controllers */





angular.module('app')
	.controller("EntityModalCtrl", ["$scope", "$modalInstance","Auth", "FirebaseSvc","entity",
    function ($scope, $modalInstance, Auth, FirebaseSvc, entity) {
      $scope.entity = entity;
      console.log(entity);
      $scope.ok = function () {
        $modalInstance.close($scope.entity);
      };

      $scope.cancel = function () {
          $modalInstance.dismiss("manual Close");
      };
    }
  ]);
