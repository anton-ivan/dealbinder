'use strict';





/* Controllers */
angular.module('app')
	.controller("LoginCtrl", ["$scope", "Auth",
	  function($scope, Auth) {
	    $scope.login_ = function() {
	      $scope.message = null;
	      $scope.error = null;

	      Auth.$authWithPassword({
	        email: $scope.email,
	        password: $scope.password
	      }).then(function(error, authData) {
	        location.href='./#/app/dashboard';
	      }).catch(function(error) {
	        $scope.error = error;
	      });
	    }}]);	