'use strict';

/* Controllers */



angular.module('app')

    .controller('MenuCtrl', ['$scope', '$http', '$timeout','$modal','FirebaseSvc', 'Auth','currentAuth',function($scope, $http, $timeout, $modal, FirebaseSvc,Auth, currentAuth) {
        //open user profile
        $scope.openProfileModal = function (size) {

          var modalInstance = $modal.open({
            templateUrl: 'tpl/blocks/profile_modal.html',
            controller: 'ProfileModalCtrl',
            size: 'md',
            resolve: {
              profile: function () {
                return $scope.myself;
              }
            }
          });

          modalInstance.result.then(function (result) {
            $scope.myself = result.profile;
						console.log('-----get profile update----');
						console.log(result);
						if(result.oldPassword != '' && result.newPassword != ''){
							Auth.$changePassword({
								email: $scope.myself.email,
								oldPassword: result.oldPassword,
								newPassword: result.newPassword
								}).then(function() {
								console.log("Password changed successfully!");
								}).catch(function(error) {
								console.error("Error: ", error);
							});
						}
          }, function (reason) {
            console.log(reason);
          });
        };

				//bind orders
        function bindProfile(){
          var obj = FirebaseSvc.getUserProfile();
          $scope.myself = obj;
          obj.$bindTo($scope,'myself');
          obj.$watch(function(evt){
            console.log($scope.myself);

          });
          return obj.$loaded();
        }
				$scope.$watch("myself",function(oldValue,newValue){
					offProfile = newValue;
					console.log('-----------off Profile');
					console.log(offProfile.company);
				})
				function initUser(){
					//check firebase Auth
					console.log('current Auth');
					console.log(currentAuth);
					//get firebase userid
					var userId = currentAuth.uid;
					offUUID  = userId;
					offEmail = currentAuth.email;

					FirebaseSvc.init(userId)
					.then(bindProfile);
				}
				//load profile
				initUser();
      }
		]);
