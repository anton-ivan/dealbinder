'use strict';

/* Controllers */





angular.module('app')
	.controller("OrderModalCtrl", ["$scope", "$modalInstance","Auth", "FirebaseSvc","profile",
    function ($scope, $modalInstance, Auth, FirebaseSvc,profile) {
			$scope.profile = profile;
      $scope.ok = function () {
        $modalInstance.close($scope.profile);
      };

      $scope.cancel = function () {
          $modalInstance.dismiss("manual Close");
      };
		}
  ]);
