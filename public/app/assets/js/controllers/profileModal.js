'use strict';

/* Controllers */





angular.module('app')
	.controller("ProfileModalCtrl", ["$scope", "$modalInstance","Auth", "FirebaseSvc","profile",
    function ($scope, $modalInstance, Auth, FirebaseSvc,profile) {
			$scope.profile = profile;
			$scope.oldPassword = '';
			$scope.newPassword = '';
      $scope.ok = function () {
				var result = {
					profile: profile,
					oldPassword: $scope.oldPassword,
					newPassword: $scope.newPassword
				}
        //$modalInstance.close($scope.profile);
        $modalInstance.close(result);
      };

      $scope.cancel = function () {
          $modalInstance.dismiss("manual Close");
      };
		}
  ]);
