'use strict';

/* Controllers */





angular.module('app')
	.controller("RegisterCtrl", ["$scope", "Auth","FirebaseSvc",
	  function($scope, Auth,FirebaseSvc) {
	    $scope.createUser = function() {
	      $scope.message = null;
	      $scope.error = null;

				var userData = {
					email: $scope.email,
					fname: $scope.fname,
					lname: $scope.lname
				};
				console.log('----user info');
				console.log(userData);
	      Auth.$createUser({
	        email: $scope.email,
	        password: $scope.password
	      }).then(function(udid) {
					FirebaseSvc.init(udid.uid)
					.then(FirebaseSvc.createUser(userData))
					.then(function(data){
						console.log(data);
							location.href='./#/app/dashboard';
					})
					.catch(function(error){
						console.log(error);
					});
	      }).catch(function(error) {
	        $scope.error = error;
					console.log($scope.error);
	      });
	    }
		}]);
