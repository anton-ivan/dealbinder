'use strict';

/* Controllers */





angular.module('app')
	.controller("UserCtrl", ["$scope", "Auth", "FirebaseSvc",
	  function($scope, Auth, FirebaseSvc) {
	    $scope.createUser = function() {
	      $scope.message = null;
	      $scope.error = null;

	      Auth.$createUser({
	        email: $scope.email,
	        password: $scope.password
	      }).then(function(userData) {
	        location.href='./#/app/dashboard'
	      }).catch(function(error) {
	        $scope.error = error;
	      });

        //read user profile...

        //sync user profile

        //test
        $scope.saveProfile = function() {
					alert('test');
				};
	    }}]);
