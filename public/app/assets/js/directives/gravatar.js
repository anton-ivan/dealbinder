angular.module('app')
.directive('gravatar',['md5', function (md5) {

  var defaultGravatarUrl = "http://www.gravatar.com/avatar/000?s=200";
  var regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  function getGravatarUrl(email) {
    if (!regex.test(email))
      return defaultGravatarUrl;

    return 'http://www.gravatar.com/avatar/' + md5.createHash(email) + ".jpg?s=200";
  }

  function linker(scope) {
    scope.url = getGravatarUrl(scope.email);
    console.log(scope.url);
    scope.$watch('email', function (newVal, oldVal) {
      if (newVal !== oldVal) {
        scope.url = getGravatarUrl(scope.email);
      }
    });
  }

  return {
    template: '<img ng-src="{{url}}"  ui-jq="unveil"  width="32" height="32">',
    restrict: 'EA',
    replace: true,
    scope: {
      email: '='
    },
    link: linker
  };

}]);
