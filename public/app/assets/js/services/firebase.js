/**
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!! --> READ THIS <-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!
 *
 * This service is shared with multiple applictions across our platform. Any
 * changes here to need to be non-environment specific. Be very careful
 * when updating this, as it will break things elsewhere! You've been warned.
 */
angular.module('app').factory('FirebaseSvc', function(
  $log,
  $filter,
  $firebase,
  $firebaseArray,
  $firebaseAuth,
  $firebaseObject,
  $q
) { //--------------------------------------------------------------------------


  var self = this;
  var firebaseUrl= "https://dealbinder.firebaseio.com";
  var ref = new Firebase(firebaseUrl);
  var priv = {}; // Holds private methods and vars

  // Public references to firebase
  this.ref = ref;
  this.authRef = $firebaseAuth(ref);

  // Flag for
  this.readyState = 'uninitialized';

  // Set private vars
  this.init = function(userId) {
    if (!userId) return $q.reject('no user passed to construsctor');

    // API data
    priv.userId = userId;


    // Firebase locations
    priv.base = ref;
    priv.users = ref.child('users/'+userId);
    priv.orders = priv.users.child('orders');
    priv.company = priv.users.child('company');

    // Let the controllers know when loaded
    self.readyState = 'completed';

    return $q.when();
  };

  /**
   * Turn a firebase request into a promise
   */
  priv.promisify = function(ref) {
    $def = $q.defer();
    ref.on('value', function() {
      $def.resolve();
    }, function(err) {
      $def.reject(err);
    });
    return $def.promise;
  };


  //----------------------------------------------------------------------------
  // THREADS
  //----------------------------------------------------------------------------

  this.setUserId = function(userId) {
    priv.userId = userId;
    return $q.when();
  };


  //get threadfor consumer to businss user communication
  this.getOrders = function(orderId) {
    var ref = priv.orders;
    return $firebaseObject(ref);
  };
  //get user Profile
  this.getUserProfile = function() {
    var ref = new Firebase(priv.base +'/users/'+ priv.userId);
    return $firebaseObject(ref);
  };



  this.setThreadStatus = function(threadId, status) {
    var ref = new Firebase(priv.base +'/threads/'+ priv.org_id +'/'+ threadId);
    ref.on('value', function(snapshot) {
      if (snapshot.val() !== null) {
        ref.update({
          'status': status
        });
      }
    });
  };


  this.createUser = function( userData) {
    var $def = $q.defer();

    var ref = new Firebase(priv.base +'/users/'+ priv.userId);
    var newUser = {
      email: userData.email,
      fname: userData.fname,
      lname: userData.lname,
      created: Firebase.ServerValue.TIMESTAMP
    };
    var obj = new $firebaseObject(ref);
    obj.$value = newUser;
    obj.$save()
    .then(function(){
      $def.resolve(ref.$value);
    }).catch(function(err){
      $def.reject(err);
    });

    return $def.promise;
  };
  
    // Set the last message on the thread
  this.updateUserProfile = function(userData) {
    // Update the thread
    var threadRef = new Firebase(priv.base +'/users/');
    threadRef.child(priv.userId).update({
      updated: Firebase.ServerValue.TIMESTAMP,
      fname:  userData.firstName,
      lname:  userData.lastName,
      phone:  userData.phone,
      status: 'active'
    });
    threadRef.setPriority(Firebase.ServerValue.TIMESTAMP);
    return priv.promisify(threadRef);
  };


  this.addSelfPresence = function() {
    if (!priv.user) return $q.reject('Not authenticated');
    var connectedRef = new Firebase(priv.base +'/.info/connected');
    console.log('adding self presence...');
    // My presence
    _myPresenceRef = new Firebase(priv.base +'/presence/' + priv.user._id);
    var lastOnlineRef = new Firebase(priv.base +'/users/' + priv.user._id +'/last_online');
    // Org presence (access allowed from org users only)
    _myOrgPresenceRef = new Firebase(priv.base +'/org/' + priv.org_id +'/connections');
    connectedRef.on('value', function(snap) {
      if (snap.val() === true) {
        // Push to /connections when online ()
        var con = _myPresenceRef.push(true);
        var key = con.key();
        con.onDisconnect(true).remove();

        // Push to org
        con.on('value', function() {
          var orgCon = _myOrgPresenceRef.push(true);
          orgCon.onDisconnect(true).remove();
        });

        // When I disconnect, set  last time I was seen online, remove /connections
        lastOnlineRef.onDisconnect().set(Firebase.ServerValue.TIMESTAMP);
      }
    });
  };
  //----------------------------------------------------------------------------
  // UTILITIES
  //----------------------------------------------------------------------------

  this.copyRecord = function(oldRef, newRef) {
    oldRef.once('value', function(snap)  {
      newRef.set( snap.value(), function(error) {
        if( error && typeof(console) !== 'undefined' && console.error ) {  console.error(error); }
      });
    });
  };

  this.moveRecord = function(oldRef, newRef) {
    oldRef.once('value', function(snap)  {
      newRef.set( snap.value(), function(error) {
        if( !error ) {  oldRef.remove(); }
        else if( typeof(console) !== 'undefined' && console.error ) {  console.error(error); }
      });
    });
  };

  return self;

});
